package com.tedneward.example;

import java.beans.*;
import java.util.*;

public class Person implements Comparable<Person>  {
    private int age;
    private String name;
    private double salary;
    private String ssn;
    private boolean propertyChangeFired = false;
    private static int count;

    static class AgeComparator implements Comparator <Person> {
        @Override
        public int compare(Person p1, Person p2)
        {
            return p1.age - p2.age;
        }
    }

    public Person() {
        this("", 0, 0.0d);
    }

    public Person(String n, int a, double s) {
        name = n;
        age = a;
        salary = s;
        ssn = "";
        count++;
    }

    @Override
    public int compareTo(Person other) {
        if (this.salary < other.getSalary()) {
            return 1;
    }
        if (this.salary > other.getSalary()) {
            return -1;
    }
        return 0;
    }

    public static ArrayList<Person> getNewardFamily() {
        ArrayList<Person> newardFamily = new ArrayList();
        Person p1 = new Person ("Ted", 41, 250000);
        Person p2 = new Person ("Charlotte", 43, 150000);
        Person p3 = new Person ("Michael", 22, 10000);
        Person p4 = new Person ("Matthew", 15, 0);
        newardFamily.add(p1);
        newardFamily.add(p2);
        newardFamily.add(p3);
        newardFamily.add(p4);
        return newardFamily;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int x){
        if (x < 0) {
            throw new IllegalArgumentException();
        }
        int old = age;
        age = x;
    }

    public String getName() {
        return name;
    }

    public void setName(String n){
        if (n == null) {
            throw new IllegalArgumentException();
        }
        String old = name;
        name = n;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double s) {
        Double old = salary;
        salary = s;
    }

    public String getSSN() {
        return ssn;
    }

    public void setSSN(String value) {
        String old = ssn;
        ssn = value;

        this.pcs.firePropertyChange("ssn", old, value);
        propertyChangeFired = true;
    }
    public boolean getPropertyChangeFired() {
        return propertyChangeFired;
    }

    public double calculateBonus() {
        return salary * 1.10;
    }

    public String becomeJudge() {
        return "The Honorable " + name;
    }

    public int timeWarp() {
        return age + 10;
    }

     public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof Person) {
            Person that = (Person) other;
            result = (this.getAge() == that.getAge() && this.getName() == that.getName());
        }
        return result;
    }

    public String toString() {
         return "[Person name:" + name + " age:" + age + " salary:" + salary + "]";
    }

    public int count(){
        return count;
    }


    // PropertyChangeListener support; you shouldn't need to change any of
    // these two methods or the field
    //
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }
}
